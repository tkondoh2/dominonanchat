let Socket = require('socket.io');
let cookie = require('cookie');
let cookieParser = require('cookie-parser');
let NodeChat = require('./NodeChat');
let io;

module.exports = function (server) {
  io = Socket.listen(server);

  let getUser = function (socket) {
    return new Promise(function (resolve) {
      let cookieStr = socket.handshake.headers.cookie.split('; ');
      let cookieJson = {};
      for (str of cookieStr) {
        let jsonAry = str.split('=');
        cookieJson[jsonAry[0]] = jsonAry[1]
      }
      resolve(cookieJson)
    });
  };

  /**
   * ソケットサーバーに接続要求が来たら処理します。
   */
  io.on('connect', function (socket) {

    // ソケットクライアントに'chat'リスナーを追加します。
    socket.on('chat', function (data) {
      getUser(socket)
        .then(async function (result) {
          let nodeChat = await new NodeChat(result.OAuth2UserName, result.OAuth2AccessToken);
          let user = await nodeChat.chat(data);
          data.username = decodeURIComponent(result.OAuth2UserName);
          io.sockets.emit('chat', data);
        })
        .catch(function (err) {
          console.log(err);
        });

    });

    // チャットの内容をリロードします。
    socket.on('reload', function (input) {
      getUser(socket)
        .then(async function (result) {
          let nodeChat = await new NodeChat(result.OAuth2UserName, result.OAuth2AccessToken);
          let data = await nodeChat.reload(input)
          socket.emit('refresh', data);
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  });

  return io;
};
