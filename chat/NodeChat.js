const config = require('config')
const request = require('request')
const postURL = config.dominoHome + config.db.filePath + '/api/data/documents'
const getURL = config.dominoHome + config.db.filePath + '/api/data/collections/name/($All)'

// RESTクライアントモジュールを使用します。
let Client = require('node-rest-client-promise').Client;
let client = ''
let user;
let parms;

// NodeChatクラスを新設します。
function NodeChat(userName, accessToken) {
  try {
    let options = {
      connection: {
        'rejectUnauthorized': false,
        'headers': {
          'Authorization': `Bearer ${accessToken}`
        }
      }
    };
    this.client = new Client(options);
    user = userName;
  } catch (err) {
    console.log(err);
    parms.message = 'Error retrieving messages';
    parms.error = { status: `${err.code}: ${err.message}` };
    parms.debug = JSON.stringify(err.body, null, 2);
    res.render('error', parms);
  }

}

// チャットデータをNSFにPOSTします。
NodeChat.prototype.chat = async function (data) {
  try {
    let cliObj = this.client;
    let args = {
      'parameters': {
        'form': 'MainTopic',
        'computewithform': true
      },
      'headers': {
        'Content-Type': 'application/json'
      },
      'data': {
        'Subject': data.message
      }
    };
    let rawData = await cliObj.postPromise(postURL, args);
    return user
  } catch (err) {
    console.log(err);
    parms.message = 'Error retrieving messages';
    parms.error = { status: `${err.code}: ${err.message}` };
    parms.debug = JSON.stringify(err.body, null, 2);
    res.render('error', parms);
  }
}

// 現在のチャットデータすべてを取得します。
NodeChat.prototype.reload = async function (input) {
  try {
    let cliObj = this.client;
    let args = {
      'parameters': {
        'compact': true,
        'start': input.start,
        'count': input.count
      }
    };
    let retData = await cliObj.getPromise(getURL, args);

    if (retData.response.statusCode && retData.response.statusCode >= 400)
      throw new Error(retData)
    else {
      let retParam = {
        items: retData.data,
        range: retData.response.headers['content-range']
      }
      return retParam;
    }
  } catch (err) {
    console.log(err);
    parms.message = 'Error retrieving messages';
    parms.error = { status: `${err.code}: ${err.message}` };
    parms.debug = JSON.stringify(err.body, null, 2);
    res.render('error', parms);
  }
}


module.exports = NodeChat;
