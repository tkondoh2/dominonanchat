(function()
{
  // クライアント側のソケットを初期化します。
  var socket = io.connect(location.origin);
  
  // 送信ボタンをクリックしたときの操作を定義します。
  $('button#send').click(function(){
    var msg = $('#message').val();
    socket.emit('chat', {
      message: msg
    });
    $('#message').val('');
  });

  // Enterキーにも対応させる。
  $('input#message').keypress(function(event){
    if (event.which == 13)
      $('button#send').click();
  });

  // チャットメッセージを受信します。
  socket.on('chat', function(data){
    var isCN = data.username.match('/CN/g');
    var isSla = data.username.match('///g');
    $('#output').append(
      '<p>'
        + '<strong>' + data.username + '</strong>: '
      + data.message + '</p>'
    );
    $("#chat-window").scrollTop( $("#chat-window")[0].scrollHeight );
  });

  // リフレッシュメッセージを受信します。
  socket.on('refresh', function(data){
    var range = data.range.match(/items (\d+)-(\d+)\/(\d+)/);
    var start = parseInt(range[1]);
    var end = parseInt(range[2]);
    var allCount = parseInt(range[3]);
    if (start === 0)
      $('#output').empty();
    data.items.forEach(function(item)
    {
      var content = item['$120'];
      var message = content.match(/(.*) \(/);
      var username = content.match(/ \((.*)\)/);
      $('#output').prepend(
        '<p>'
          + '<strong>' + username[1] + '</strong>: '
        + message[1] + '</p>'
      );
      $("#chat-window").scrollTop( $("#chat-window")[0].scrollHeight );
    });
    if (end + 1 < allCount)
      socket.emit('reload', {'start': end + 1, 'count': countUnit});
  });

  // チャットの履歴を取得します。
  var countUnit = 10;
  socket.emit('reload', {'start': 0, 'count':countUnit});

})()
