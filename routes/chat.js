const config = require('config')

// Dominoからデータを取得するURL
const GET_DOMINO_DATA_PATH = config.db.filePath + '/api/data/documents';

const NodeChat = require('../chat/NodeChat');
const express = require('express');
const authHelper = require('../helpers/auth');
const Client = require('node-rest-client-promise').Client;

var io;

module.exports = {
  setIo: function (socket_io) {
    io = socket_io;
  },

  router: function () {
    var router = express.Router();

    /* GET /chat */
    router.get('/', function (req, res, next) {
      let parms = { title: 'Chat', active: { inbox: true } };
      let accessToken = req.cookies.OAuth2AccessToken;
      let userName = req.cookies.OAuth2UserName;

      if (accessToken && userName) {
        parms.user = userName;
        res.render('chat', parms);
      } else {
        res.redirect('/');
      }
    });

    router.post('/', function (req, res) {
      io.sockets.emit('chat', req.body);
      res.send('OK');
    });

    return router;
  }

}


